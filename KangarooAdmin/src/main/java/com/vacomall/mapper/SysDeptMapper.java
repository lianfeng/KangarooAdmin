package com.vacomall.mapper;

import com.vacomall.entity.SysDept;
import com.baomidou.mybatisplus.mapper.AutoMapper;

/**
 *
 * SysDept 表数据库控制层接口
 *
 */
public interface SysDeptMapper extends AutoMapper<SysDept> {


}
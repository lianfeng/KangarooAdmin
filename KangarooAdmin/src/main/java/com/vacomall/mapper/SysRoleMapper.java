package com.vacomall.mapper;

import com.vacomall.entity.SysRole;
import com.baomidou.mybatisplus.mapper.AutoMapper;

/**
 *
 * SysRole 表数据库控制层接口
 *
 */
public interface SysRoleMapper extends AutoMapper<SysRole> {


}
package com.vacomall.mapper;

import com.vacomall.entity.SysSetting;
import com.baomidou.mybatisplus.mapper.AutoMapper;

/**
 *
 * SysSetting 表数据库控制层接口
 *
 */
public interface SysSettingMapper extends AutoMapper<SysSetting> {


}
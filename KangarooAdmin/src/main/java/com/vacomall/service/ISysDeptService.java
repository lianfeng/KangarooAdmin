package com.vacomall.service;

import com.vacomall.entity.SysDept;
import com.baomidou.framework.service.ISuperService;

/**
 *
 * SysDept 表数据服务层接口
 *
 */
public interface ISysDeptService extends ISuperService<SysDept> {


}
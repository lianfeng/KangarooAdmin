package com.vacomall.service;

import com.vacomall.entity.SysRole;
import com.baomidou.framework.service.ISuperService;

/**
 *
 * SysRole 表数据服务层接口
 *
 */
public interface ISysRoleService extends ISuperService<SysRole> {


}
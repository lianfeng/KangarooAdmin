package com.vacomall.service.impl;

import org.springframework.stereotype.Service;

import com.vacomall.mapper.SysDeptMapper;
import com.vacomall.entity.SysDept;
import com.vacomall.service.ISysDeptService;
import com.vacomall.service.support.BaseServiceImpl;

/**
 *
 * SysDept 表数据服务层接口实现类
 *
 */
@Service
public class SysDeptServiceImpl extends BaseServiceImpl<SysDeptMapper, SysDept> implements ISysDeptService {


}
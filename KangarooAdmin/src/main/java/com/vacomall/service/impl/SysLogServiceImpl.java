package com.vacomall.service.impl;

import org.springframework.stereotype.Service;

import com.vacomall.mapper.SysLogMapper;
import com.vacomall.entity.SysLog;
import com.vacomall.service.ISysLogService;
import com.vacomall.service.support.BaseServiceImpl;

/**
 *
 * SysLog 表数据服务层接口实现类
 *
 */
@Service
public class SysLogServiceImpl extends BaseServiceImpl<SysLogMapper, SysLog> implements ISysLogService {


}
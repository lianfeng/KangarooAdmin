package com.vacomall.service.impl;

import org.springframework.stereotype.Service;

import com.vacomall.mapper.SysRoleMapper;
import com.vacomall.entity.SysRole;
import com.vacomall.service.ISysRoleService;
import com.vacomall.service.support.BaseServiceImpl;

/**
 *
 * SysRole 表数据服务层接口实现类
 *
 */
@Service
public class SysRoleServiceImpl extends BaseServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {


}